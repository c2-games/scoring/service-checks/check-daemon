package database

import (
	"encoding/json"
	"time"
)

// Team maps to the GQL table public.teams
type Team struct {
	ID              string  `json:"id"`
	CompetitionID   *int32  `json:"competition_id"`
	Name            string  `json:"name"`
	LongName        *string `json:"long_name"`
}

// Event maps to the GQL table public.events
type Event struct {
	ID    string     `json:"id"`
	Name  string     `json:"name"`
	Start *time.Time `json:"start"`
	End   *time.Time `json:"end"`
}

// Check maps to the GQL table checks.checks
type Check struct {
	ID          string    `json:"id"`
	Type        string    `json:"type"`
	Name        string    `json:"name"`
	DisplayName string    `json:"display_name"`
	Script      string    `json:"script"`
	Args        string    `json:"args"`
	Created     time.Time `json:"created"`
	Modified    time.Time `json:"modified"`
	Description string    `json:"description"`
}

// NestedVariables is a generic variables with ID struct
// This struct maps to the data in each of the variables schemas
type NestedVariables struct {
	ID   string           `json:",omitempty"`
	Vars *json.RawMessage `json:"vars"`
}

// EventVariables holds event specific variables
type EventVariables NestedVariables

// CheckVariables holds check specific variables
type CheckVariables NestedVariables

// TeamVariables holds team specific variables
type TeamVariables NestedVariables

// DeploymentVariables holds deployment specific variables
type DeploymentVariables NestedVariables

// DeployedCheck maps to the GQL table checks.deployed
type DeployedCheck struct {
	ID             string               `json:"id"`
	CheckID        string               `json:"check_id"`
	EventID        string               `json:"event_id"`
	TeamID         string               `json:"team_id"`
	Check          Check                `json:"check"`
	Event          Event                `json:"event"`
	Team           Team                 `json:"team"`
	EventVars      *EventVariables      `json:"event_vars,omitempty"`
	CheckVars      *CheckVariables      `json:"check_vars,omitempty"`
	TeamVars       *TeamVariables       `json:"team_vars,omitempty"`
	DeploymentVars *DeploymentVariables `json:"deployment_vars,omitempty"`
}
