package database

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"

	"check-daemon/authentication"
	"check-daemon/config"

	"go.uber.org/zap"
)

var (
	loggerBasic *zap.Logger
	logger      *zap.SugaredLogger
	cfg         *config.DatabaseConfig
)

// SetLogger sets up the internal logger for the gql package
func SetLogger(loggerIn *zap.Logger) {
	loggerBasic = loggerIn
	logger = loggerBasic.Sugar()
}

// SetConfig sets up the internal config for the gql package
func SetConfig(cfgIn *config.DatabaseConfig) {
	cfg = cfgIn
}

// GQLQuery will take a query string, marshal it into a valid JSON data structure,
// send it to the graphql endpoint and return the marshalled response.
func GQLQuery(query string, variables map[string]interface{}, role string) (r []byte, e error) {
	q := map[string]interface{}{"query": query}
	if variables != nil {
		q["variables"] = variables
	}
	j, _ := json.Marshal(q)

	logger.Debugf("query using %v role", role)
	data, err := httpGQLQuery(j, role)

	if err != nil {
		logger.With(err).Errorf("error querying GraphQL")
		return nil, err
	}

	err = validateGqlResp(data)
	if err != nil {
		logger.With(err).Errorf("error validating GraphQL response data")
		return nil, err
	}

	return data, nil
}

func httpGQLQuery(buf []byte, role string) ([]byte, error) {
	req, _ := http.NewRequest("POST", cfg.GqlURI, bytes.NewBuffer(buf))
	if role != cfg.UnauthenticatedRole {
		token, err := authentication.GetClientAuthToken()
		if err != nil {
			logger.With(err).Debug("error retrieving client token")
			return nil, err
		}
		if !token.Valid() {
			return nil, fmt.Errorf("client token returned is invalid")
		}
		token.SetAuthHeader(req) // Add in the Bearer token to the request
		req.Header.Set("x-hasura-role", role)
	}
	client := &http.Client{
		Timeout: cfg.RequestTimeout,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: cfg.TLSSkipVerify,
			},
		},
	}
	resp, err := client.Do(req)

	if err != nil {
		logger.With(err).Errorf("GQL http request failed")
		return nil, err
	}
	defer resp.Body.Close()
	return io.ReadAll(resp.Body)
}

func validateGqlResp(objJSON []byte) error {
	var result map[string]interface{}

	err := json.Unmarshal([]byte(objJSON), &result)
	if err != nil {
		return err
	}

	if result["errors"] != nil {
		// Accommodate for more than one error
		errMsgs := "gql errors"
		for num, item := range result["errors"].([]interface{}) {
			var errMsg string
			msgIface, ok := item.(map[string]interface{})
			if !ok {
				logger.Debug("error from GQL does not cast to map[string]interface{}")
				errMsg = "error casting object"
			} else {
				errMsg, ok = msgIface["message"].(string)
				if !ok {
					logger.Debug("error from GQL does not contain \"message\" entry")
					errMsg = "error casting message"
				}
			}
			logger.Debugf("error from gql %v: %v\n", num, errMsg)
			errMsgs = fmt.Sprintf("%s; %s", errMsgs, errMsg)
		}

		return errors.New(errMsgs)
	}

	return nil
}
