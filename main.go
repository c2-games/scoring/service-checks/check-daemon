/*
Copyright © 2022 C2Games <support@c2games.org>
*/
package main

import "check-daemon/cmd"

func main() {
	cmd.Execute()
}
