package daemon

import (
	"bytes"
	"check-daemon/checks"
	"check-daemon/database"
	"check-daemon/util"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"html/template"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
	"sync"
	"time"
	"unicode/utf8"
)

type tmplExecParams struct {
	Cmd      string
	Args     string
	Team     database.Team
	Host     string
	Check    database.Check
	VarsFile string
}

type tmplArgParams struct {
	Team     database.Team
	Host     string
	Check    database.Check
	VarsFile string
}

type cmdResult struct {
	Result       string                 `json:"result"`
	ExtendedData map[string]interface{} `json:"extendedData"`
}

type execInfo struct {
	Cmd           string    `json:"cmd"`
	Args          []string  `json:"args"`
	Errors        []string  `json:"errors"`
	Stdout        string    `json:"stdout"`
	Stderr        string    `json:"stderr"`
	StartTime     time.Time `json:"start_time"`
	PostDelayTime time.Time `json:"post_delay_time"`
	EndTime       time.Time `json:"end_time"`
}

func (d *Daemon) makeCheckCommandArgs(check database.DeployedCheck, varsFile string) ([]string, error) {
	var (
		cmdArgs     []string
		execArgsStr bytes.Buffer
		args        bytes.Buffer
	)
	execTmpl, err := template.New("cmd").Parse(d.Config.Check.ArgsTmpl)
	if err != nil {
		return nil, fmt.Errorf("could not parse template: %s", err.Error())
	}
	argsTmpl, err := template.New("args").Parse(check.Check.Args)
	if err != nil {
		return nil, fmt.Errorf("could not parse args template: %s", err.Error())
	}
	err = argsTmpl.Execute(&args,
		tmplArgParams{
			Team:     check.Team,
			Check:    check.Check,
			VarsFile: varsFile,
		})
	if err != nil {
		return nil, fmt.Errorf("could not template args: %s", err.Error())
	}
	script := path.Join(d.Config.Check.ScriptsDir, check.Check.Script)
	err = execTmpl.Execute(
		&execArgsStr,
		tmplExecParams{
			Cmd:      script,
			Args:     args.String(),
			Team:     check.Team,
			Check:    check.Check,
			VarsFile: varsFile,
		},
	)
	if err != nil {
		return nil, fmt.Errorf("could not template command: %s", err.Error())
	}
	cmdArgs = append(cmdArgs, d.Config.Check.RunCmd[1:]...)
	cmdArgs = append(cmdArgs, execArgsStr.String())
	return cmdArgs, nil
}

func setupWorkDir(check database.DeployedCheck) (string, string, error) {
	var (
		varsFile string
		tempDir  string
	)
	// TODO: slugify check.Team.Name for easier debugging?
	tempDir, err := os.MkdirTemp(os.TempDir(), "check_"+check.Check.Name+"_"+check.Team.ID+"_")
	if err != nil {
		return tempDir, varsFile, err
	}
	varsFile = filepath.Join(tempDir, "vars.json")
	checkVars := []checks.CheckVariable{}
	if nil != check.EventVars {
		checkVars = append(checkVars, checks.CheckVariable{Prefix: "event", Content: *check.EventVars.Vars})
	}
	if nil != check.CheckVars {
		checkVars = append(checkVars, checks.CheckVariable{Prefix: "check", Content: *check.CheckVars.Vars})
	}
	if nil != check.TeamVars {
		checkVars = append(checkVars, checks.CheckVariable{Prefix: "team", Content: *check.TeamVars.Vars})
	}
	if nil != check.DeploymentVars {
		checkVars = append(checkVars, checks.CheckVariable{Prefix: "deployment", Content: *check.DeploymentVars.Vars})
	}
	// Add in team info from database
	teamJSON, _ := json.Marshal(check.Team)
	checkVars = append(checkVars, checks.CheckVariable{Prefix: "team_metadata", Content: json.RawMessage(teamJSON)})
	// Serialize all vars into a single JSON output
	jsonBytes, err := checks.SerializeVariables(checkVars, false)
	if err == nil {
		err = os.WriteFile(varsFile, jsonBytes, 0444)
	} else {
		// Delete the tempDir if the variables file couldn't be setup, let the error boil up
		rmErr := os.RemoveAll(tempDir)
		if rmErr != nil {
			logger.Warnf("could not remove temp directory %s: %s", tempDir, rmErr.Error())
		}
	}
	return tempDir, varsFile, err
}

func recursiveDeleteDir(name string, trigger bool) {
	if !trigger {
		rmErr := os.RemoveAll(name)
		if rmErr != nil {
			logger.Warnf("could not remove temp directory %s: %s", name, rmErr.Error())
		} else {
			logger.Debugf("recursively removed temp directory %s", name)
		}
	} else {
		logger.Warnf("no clean up configured so not cleaning up directory %s", name)
	}
}

func (d *Daemon) execCmd(check database.DeployedCheck, execInfo *execInfo) ([]byte, []byte, bool, error) {
	var (
		ctx    context.Context
		cancel context.CancelFunc
		cmd    *exec.Cmd
	)
	var retErr error = nil
	var timedOut bool = false
	tempDir, varsFile, err := setupWorkDir(check)
	if err != nil {
		return nil, nil, timedOut, fmt.Errorf("could not setup temp directory: %s", err.Error())
	}
	logger.Debugf(
		"created temp directory %s with vars file %s for team %s",
		tempDir, varsFile, check.Team.Name,
	)
	// Make sure temp dir gets cleaned up if it is marked for clean up
	defer recursiveDeleteDir(tempDir, d.Config.Setup.DebugNoCleanUp)

	command := d.Config.Check.RunCmd[0]
	timeout := d.Config.Check.Timeout
	args, err := d.makeCheckCommandArgs(check, varsFile)
	if err != nil {
		return nil, nil, timedOut, err
	}
	logger.Infof(
		"executing %s %s: %s %s",
		check.Team.Name,
		check.Check.Name,
		command,
		strings.Join(args, " "),
	)
	if timeout == 0*time.Second {
		cmd = exec.Command(command, args...)
	} else {
		ctx, cancel = context.WithTimeout(context.Background(), timeout)
		cmd = exec.CommandContext(ctx, command, args...)
		defer cancel()
	}
	// Execute the command in the temp directory
	cmd.Dir = tempDir
	stdout := new(bytes.Buffer)
	cmd.Stdout = stdout
	stderr := new(bytes.Buffer)
	cmd.Stderr = stderr
	execInfo.Cmd = command
	execInfo.Args = args
	err = cmd.Run()
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			// Check if ctx is nil because if timeout is 0, it isn't allocated
			if ctx != nil && ctx.Err() != nil {
				retErr = fmt.Errorf("timeout reached executing command: %s", ctx.Err().Error())
				timedOut = true
			} else {
				retErr = fmt.Errorf("exception while executing command: %s", exitError.String())
			}
		} else {
			retErr = fmt.Errorf("exception while executing command: %s", err.Error())
		}
	}
	return stdout.Bytes(), stderr.Bytes(), timedOut, retErr
}

func (d *Daemon) execCheck(check database.DeployedCheck) checkResult {
	var (
		execInfo     execInfo
		resultCode   string
		cmdResult    cmdResult
		extendedData *json.RawMessage
		staff        *json.RawMessage
		participant  *json.RawMessage
	)

	execInfo.StartTime = time.Now()
	checkTime := time.Now()
	d.sleepRandomDelay(check)
	execInfo.PostDelayTime = time.Now()
	stdout, stderr, timedOut, err := d.execCmd(check, &execInfo)
	execInfo.EndTime = time.Now()
	resultCode = "error"
	if utf8.Valid(stderr) {
		execInfo.Stderr = string(stderr[:])
	} else {
		execInfo.Stderr = base64.StdEncoding.EncodeToString(stderr)
	}
	if utf8.Valid(stdout) {
		execInfo.Stdout = string(stdout[:])
	} else {
		execInfo.Stdout = base64.StdEncoding.EncodeToString(stdout)
	}
	if err != nil {
		execInfo.Errors = append(execInfo.Errors, err.Error())
	} else {
		err = json.Unmarshal(stdout, &cmdResult)
		if err != nil {
			execInfo.Errors = append(
				execInfo.Errors,
				fmt.Sprintf("could not unmarshal stdout: %s", err.Error()),
			)
		} else {
			resultCode = cmdResult.Result
			if _, ok := cmdResult.ExtendedData["participant"]; ok {
				p, err := json.Marshal(cmdResult.ExtendedData["participant"])
				if err != nil {
					execInfo.Errors = append(
						execInfo.Errors,
						fmt.Sprintf("could not unmarshal participant: %s", err.Error()),
					)
				} else {
					tmp := json.RawMessage(p)
					participant = &tmp
				}
				delete(cmdResult.ExtendedData, "participant")
			} else {
				execInfo.Errors = append(
					execInfo.Errors,
					"no \"participant\" field in output",
				)
			}
			if _, ok := cmdResult.ExtendedData["staff"]; ok {
				s, err := json.Marshal(cmdResult.ExtendedData["staff"])
				if err != nil {
					execInfo.Errors = append(
						execInfo.Errors,
						fmt.Sprintf("could not unmarshal staff: %s", err.Error()),
					)
				} else {
					tmp := json.RawMessage(s)
					staff = &tmp
				}
				delete(cmdResult.ExtendedData, "staff")
			} else {
				execInfo.Errors = append(
					execInfo.Errors,
					"no \"staff\" field in output",
				)
			}
		}
	}
	if nil == cmdResult.ExtendedData {
		cmdResult.ExtendedData = map[string]interface{}{}
	}
	execRaw, _ := json.Marshal(execInfo)
	cmdResult.ExtendedData["exec"] = json.RawMessage(execRaw)
	edb, _ := json.Marshal(cmdResult.ExtendedData)
	if len(edb) > 0 {
		tmp := json.RawMessage(edb)
		extendedData = &tmp
	}
	if timedOut {
		resultCode = "timeout"
		if nil == participant {
			type Fake struct {
				Feedback        string          `json:"feedback"`
				DetailedResults json.RawMessage `json:"detailed_results"`
			}
			fb, err := json.Marshal(Fake{Feedback: "Timeout"})
			if err != nil {
				logger.Warn("could not fabricate participant for script timeout")
			} else {
				rawMsg := json.RawMessage(fb)
				participant = &rawMsg
			}
		}
	}
	return checkResult{
		EventID:     check.EventID,
		CheckID:     check.CheckID,
		TeamID:      check.TeamID,
		Target:      "divined",
		Source:      d.getIpv4Address(),
		CheckTime:   checkTime,
		ResultCode:  resultCode,
		Data:        extendedData,
		Participant: participant,
		Staff:       staff,
	}
}

func (d *Daemon) sleepRandomDelay(check database.DeployedCheck) {
	// Generate a random amount of jitter in service checks
	jitter := util.GenerateRandom(
		int(d.Config.Check.DelayMin.Seconds()),
		int(d.Config.Check.DelayMax.Seconds()),
	)
	logger.Debugf(
		"waiting %d seconds before executing '%s' '%s' check",
		jitter, check.Team.Name,
		check.Check.Name,
	)
	time.Sleep(time.Duration(jitter) * time.Second)
}

func (d *Daemon) performCheck(c chan checkResult, check database.DeployedCheck) {
	cr := d.execCheck(check)
	// If queuing is disabled, allow direct batch result upload
	if d.queue.MaxSize == 0 {
		c <- cr
	} else {
		d.queue.Enqueue(cr)
	}
}

// RunChecks batch executes the checks passed, then uploads results to GQL
func (d *Daemon) RunChecks(checks []database.DeployedCheck) error {
	c := make(chan checkResult, len(checks))
	var wg sync.WaitGroup
	wg.Add(len(checks))
	for _, check := range checks {
		// Run all checks in parallel
		go func(check database.DeployedCheck) {
			defer wg.Done()
			d.performCheck(c, check)
		}(check)
	}
	go func() {
		wg.Wait()
		close(c)
	}()
	// If queuing is disabled, direct batch result upload
	// otherwise, let the queue upload thread take care of it
	if d.queue.MaxSize == 0 {
		var results []checkResult
		for r := range c {
			results = append(results, r)
		}
		if len(results) > 0 {
			err := d.uploadCheckData(results)
			if err != nil {
				logger.Errorf("could not upload check results: %s", err.Error())
				return err
			}
		}
		logger.Infof("%d of %d check results uploaded", len(results), len(checks))
	}
	return nil
}
