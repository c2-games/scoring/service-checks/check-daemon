package daemon

import (
	"check-daemon/authentication"
	"check-daemon/config"
	"check-daemon/database"
	"check-daemon/util"
	"check-daemon/queue"
	"net"
	"os"
	"time"

	"github.com/go-co-op/gocron"
	"go.uber.org/zap"
)

var (
	loggerBasic *zap.Logger
	logger      *zap.SugaredLogger
)

// Daemon is an instance used to run the checks
type Daemon struct {
	// configuration
	Config         *config.DaemonConfig
	// ip address of this daemon
	ipv4           *string
	// identifier for this check daemon
	identifier     *string
	// Scheduler managing check routines
	scheduler      *gocron.Scheduler
	// Check results queued for upload
	queue          *queue.Queue[checkResult]
	// Last known deployed checks configuration
	deployedChecks []database.DeployedCheck
	// Last update on deployed checks configuration
	dcLastUpdate   time.Time
}

// SetLogger sets up the logging for the daemon package
func SetLogger(loggerIn *zap.Logger) {
	loggerBasic = loggerIn
	logger = loggerBasic.Sugar()
}


func NewDaemon() *Daemon {
	d := new(Daemon)
	d.queue = queue.NewQueue[checkResult]()
	return d
}

// SetConfig sets up the configuration for the daemon instance
func (d *Daemon) SetConfig(cfg *config.DaemonConfig) {
	err := cfg.Validate()
	if err != nil {
		if d.Config == nil {
			logger.Fatalf("invalid configuration: %s", err.Error())
		} else {
			logger.Errorf("invalid configuration, not updating: %s", err.Error())
		}
		return
	}
	if cfg.Check.Interval < cfg.Check.Timeout {
		logger.Warn("timeout set larger than interval, this may cause crashes via go routine creep")
	}
	if cfg.Log.Level == "debug" {
		strCfg, err := util.PrettyStruct(cfg)
		if err == nil {
			logger.Debugf("configuration set: %s", strCfg)
		} else {
			logger.Debug("error serializing config")
		}
	}
	d.queue.MaxSize = cfg.Setup.QueueMax
	d.queue.FullStrategy = cfg.Setup.QueueFullStrategy
	d.Config = cfg
}

// StartChecks start checks on the configured interval
func (d *Daemon) StartChecks() {
	if d.scheduler == nil {
		logger.Panic("attempted to start checks before setup")
		return
	}
	logger.Debugf("scheduling checks for every %s", d.Config.Check.Interval.String())
	d.scheduler.Every(d.Config.Check.Interval).StartAt(
		time.Now().Truncate(d.Config.Check.Interval),
	).Tag("check").Do(d.Tick)
	// If queuing is enabled, start a scheduled task to do the uploads
	if (
		d.queue.MaxSize != 0 &&
		d.Config.Setup.SubmissionBatchSize != 0 &&
		d.Config.Setup.UploadInterval >= 1*time.Second) {
		logger.Debugf("scheduling check upload for every %s", d.Config.Setup.UploadInterval.String())
		d.scheduler.Every(d.Config.Setup.UploadInterval).Tag("upload").Do(d.UploadChecks)
	} else {
		logger.Infof("async uploads via queue are disabled by configuration")
	}
	d.scheduler.StartAsync()
}

// StopChecks stop all checks
func (d *Daemon) StopChecks() {
	if d.scheduler == nil {
		logger.Debug("attempted to stop checks before setup")
		return
	}
	logger.Debug("waiting for checks to complete")
	d.scheduler.RemoveByTag("check")
	logger.Debug("checks completed")
	if d.queue.MaxSize != 0 {
		logger.Debug("stop all check upload")
		d.scheduler.RemoveByTag("upload")
		logger.Debug("uploads stopped")
	}
}

func (d *Daemon) UploadChecks() int {
	count := d.Config.Setup.SubmissionBatchSize
	if d.Config.Setup.SubmissionBatchSize == 0 {
		logger.Info("submissions are disabled by configuration")
		return 0
	}
	if d.Config.Setup.SubmissionBatchSize < 0 {
		// If batch size is a negative, try to upload all queued results
		count = d.queue.Len()
	}
	batch, _ := d.queue.DequeueMulti(count)
	if len(batch) == 0 {
		logger.Info("no results waiting for upload")
		return 0
	}
	logger.Debugf(
		"attempting to upload %d results, %d waiting in queue",
		len(batch),
		d.queue.Len(),
	)
	err := d.uploadCheckData(batch)
	if err != nil {
		logger.Errorf("could not upload check results: %s", err.Error())
		logger.Infof(
			"requeuing %d results, queue size %d of %d",
			len(batch),
			d.queue.Len(),
			d.queue.MaxSize,
		)
		d.queue.EnqueueMulti(batch)
		logger.Debugf("requeue complete, queue size %d", d.queue.Len())
	} else {
		logger.Infof("uploaded %d results successfully", len(batch))
	}
	return 0
}

// Tick runs for every time step of the mainloop
func (d *Daemon) Tick() int {
	checks, err := d.queryDeployedChecks()
	if err != nil {
		logger.Errorf("query check configuration failed: %s", err.Error())
		return 0
	}
	if len(checks) == 0 {
		logger.Warn("no deployed checks detected for configuration")
		return 0
	}
	go func(checks []database.DeployedCheck) {
		err = d.RunChecks(checks)
		if err != nil {
			logger.Errorf("scheduling checks failed: %s", err.Error())
		} else {
			logger.Debugf("scheduled all checks")
		}
	}(checks)
	return 0
}

func (d *Daemon) getIdentifier() string {
	if d.identifier != nil {
		return *d.identifier
	}
	hostname, err := os.Hostname()
	if err != nil {
		logger.Panic("could not retrieve hostname")
	}
	d.identifier = new(string)
	*d.identifier = hostname
	return *d.identifier
}

func (d *Daemon) getIpv4Address() string {
	if d.ipv4 != nil {
		return *d.ipv4
	}
	ifaces, err := net.Interfaces()
	if err != nil {
		logger.Panic("could not query network interfaces: %w", err)
	}
	for _, i := range ifaces {
		if i.Name != d.Config.Check.NetIface {
			continue
		}
		addrs, err := i.Addrs()
		if err != nil {
			logger.Panic("could not query network addresses for interface %s: %w", i.Name, err)
		}
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			default:
				continue
			}
			d.ipv4 = new(string)
			*d.ipv4 = ip.String()
			break
		}
		if d.ipv4 != nil {
			break
		}
	}
	if d.ipv4 == nil {
		logger.Panic("no valid ipv4 address found for interface %s: %w", d.Config.Check.NetIface, err)
	}
	return *d.ipv4
}

// Setup the check daemon
func (d *Daemon) Setup() {
	// Setup database
	database.SetConfig(&d.Config.Database)
	// Query the database for extra setup information
	d.setupFromEnvironment()
	// Setup auth
	err := authentication.SetupOIDC(&d.Config.Authentication)
	if err != nil {
		logger.Fatalf("failed setting up authentication: %s", err.Error())
	}
	// Cache IP address and Identifier
	logger.Infof(
		"daemon setup complete identifier: %s, ipv4: %s",
		d.getIdentifier(), d.getIpv4Address(),
	)
	if d.scheduler == nil {
		d.scheduler = gocron.NewScheduler(time.UTC)
	}
	d.ScheduleSelfDestruct()
}

// Cleanup performs any cleanup required by Setup()
func (d *Daemon) Cleanup() {
	d.StopChecks()
}
