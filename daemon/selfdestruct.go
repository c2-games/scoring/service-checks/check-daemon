package daemon

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"

	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

// ptr Generic pointer constructor
func ptr[T any](v T) *T {
	return &v
}

// ScheduleSelfDestruct if enabled, set a future self-destruction
func (d *Daemon) ScheduleSelfDestruct() {
	if !d.Config.Setup.SelfDestruct {
		logger.Debug("process will not self-destruct")
		return
	}
	// Get a random variance
	v := time.Duration(rand.Float64() * float64(d.Config.Setup.SelfDestructVariance))
	// Get current time plus time to live
	t := d.Config.Setup.SelfDestructTTL
	// Coin flip for add or subtract
	if n := rand.Intn(2); n == 0 {
		t += v
	} else {
		t -= v
	}
	logger.Infof("process will self-destruct in %s, variance used: %s", t, v)
	job, err := d.scheduler.Every(t).WaitForSchedule().Tag("self-destruct").Do(d.doSelfDestruct)
	if err != nil {
		logger.Warn("could not schedule self-destruct: %s", err.Error())
	} else {
		d.scheduler.WaitForSchedule()
		job.LimitRunsTo(1)
	}
}

// StopSelfDestruct stops the self destruct task if scheduled
func (d *Daemon) StopSelfDestruct() {
	if d.scheduler == nil {
		logger.Debug("attempted to stop self-destruct before setup")
		return
	}
	logger.Debug("clearing self-destruct job")
	// Don't check error, we don't care if the job didn't exist
	_ = d.scheduler.RemoveByTag("self-destruct")
}

func (d *Daemon) doSelfDestruct() {
	d.Cleanup()
	if err := d.selfDestructK8s(); err != nil {
		logger.Warn("could not self destruct on k8s, exiting")
		os.Exit(0)
	}
	logger.Debug("self-destruct completed, process should terminate shortly")
}

func (d *Daemon) selfDestructK8s() error {
	var (
		err          error
		podName      string
		podNamespace string
		config       *rest.Config
		clientset    *kubernetes.Clientset
	)
	if podName, err = getSelfPodName(); err != nil {
		return err
	}
	if podNamespace, err = getSelfPodNamespace(); err != nil {
		return err
	}
	if config, err = rest.InClusterConfig(); err != nil {
		return err
	}
	if clientset, err = kubernetes.NewForConfig(config); err != nil {
		return err
	}
	opts := metaV1.DeleteOptions{
		GracePeriodSeconds: ptr[int64](0),
		PropagationPolicy:  ptr(metaV1.DeletePropagationBackground),
	}
	err = clientset.CoreV1().Pods(podNamespace).Delete(context.Background(), podName, opts)
	if err != nil {
		return err
	}
	return nil
}

func getSelfPodName() (string, error) {
	// This way assumes you've set the POD_NAME environment variable using the downward API.
	// env:
	// - name: POD_NAME
	//   valueFrom:
	//     fieldRef:
	//       fieldPath: metadata.name
	if name := os.Getenv("POD_NAME"); name != "" {
		return name, nil
	}
	if name, err := os.Hostname(); err == nil {
		return name, nil
	}
	return "", fmt.Errorf("could not resolve pod name")
}

func getSelfPodNamespace() (string, error) {
	// This way assumes you've set the POD_NAMESPACE environment variable using the downward API.
	// env:
	// - name: POD_NAMESPACE
	//   valueFrom:
	//     fieldRef:
	//       fieldPath: metadata.namespace
	if ns := os.Getenv("POD_NAMESPACE"); ns != "" {
		return ns, nil
	}

	// Fall back to the namespace associated with the service account token, if available
	if data, err := os.ReadFile("/var/run/secrets/kubernetes.io/serviceaccount/namespace"); err == nil {
		if ns := strings.TrimSpace(string(data)); len(ns) > 0 {
			return ns, nil
		}
	}

	return "", fmt.Errorf("could not resolve namespace")
}
