package daemon

import (
	"encoding/json"
	"fmt"
	"time"

	"check-daemon/database"
)

const deployedCheckQuery = `query DeployedChecks {
	checks_deployed(
		where: {
			_and: {
				event: {environments: {name: {_eq: %+q}}},
				check: {name: {_in: %+q}},
				team: {name: {_in: %+q}}
			}
		}
	) {
		id
		check_id
		event_id
		team_id
		check {
			id
			type
			name
			display_name
			script
			args
		}
		event {
			id
			name
		}
		team {
			id
			competition_id
			name
			long_name
		}
		event_vars {
			vars
		}
		check_vars {
			vars
		}
		team_vars {
			vars
		}
		deployment_vars {
			vars
		}
	}
}`

func (d *Daemon) queryDeployedChecks() ([]database.DeployedCheck, error) {
	query := fmt.Sprintf(
		deployedCheckQuery,
		d.Config.Database.Environment,
		d.Config.Check.Checks,
		d.Config.Check.Teams,
	)
	responseBytes, err := database.GQLQuery(query, nil, d.Config.Database.DefaultQueryRole)
	if err != nil {
		// Check if the deployed checks configuration was ever retrieved
		if d.dcLastUpdate.IsZero() {
			logger.Error(err)
			return nil, err
		}
		// Use the last known configuration in d.deployedChecks
		logger.Warnf(
			"database could not be reached, falling back on configuration from %s",
			d.dcLastUpdate.UTC().String(),
		)
	} else {
		// Parse the latest deployed checks from the db
		r := struct {
			Data struct {
				List []database.DeployedCheck `json:"checks_deployed"`
			} `json:"data"`
		}{}
		err = json.Unmarshal(responseBytes, &r)
		if err != nil {
			logger.Error(err)
			return nil, err
		}
		d.deployedChecks = r.Data.List
		d.dcLastUpdate = time.Now().UTC()
	}
	return d.deployedChecks, nil
}

const envQuery = `query EnvSettings {
	environments(where: {name: {_eq: %+q}}) {
		settings {
			keycloak_url
		}
	}
}`

// setupFromEnvironment Query the database for extra configuration settings
func (d *Daemon) setupFromEnvironment() error {
	query := fmt.Sprintf(
		envQuery,
		d.Config.Database.Environment,
	)
	responseBytes, err := database.GQLQuery(
		query,
		nil,
		d.Config.Database.UnauthenticatedRole,
	)
	if err != nil {
		logger.With(err).Panicf("database unreachable")
		return err
	}
	r := struct {
		Data struct {
			Environments []struct {
				Settings struct {
					KeycloakURL   string `json:"keycloak_url"`
				} `json:"settings"`
			} `json:"environments"`
		} `json:"data"`
	}{}
	err = json.Unmarshal(responseBytes, &r)
	if err != nil {
		logger.With(err).Panicf("error parsing environment configuration")
		return err
	}
	if 0 == len(r.Data.Environments) {
		logger.Panicf("No environments found for %s", d.Config.Database.Environment)
	}
	url := r.Data.Environments[0].Settings.KeycloakURL
	if len(url) < 1 {
		logger.Fatalf("Database environment settings does not contain valid OIDC URL: %s", url)
	}
	d.Config.Authentication.OIDCCfgURL = url
	logger.Debugf("openid-configuration URL: %s", d.Config.Authentication.OIDCCfgURL)
	return nil
}
