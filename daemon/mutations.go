package daemon

import (
	"check-daemon/database"
	"encoding/json"
	"time"
)

type checkResult struct {
	EventID     string           `json:"event_id"`
	CheckID     string           `json:"check_id"`
	TeamID      string           `json:"team_id"`
	Target      string           `json:"target"`
	Source      string           `json:"source"`
	CheckTime   time.Time        `json:"check_time"`
	ResultCode  string           `json:"result_code"`
	Data        *json.RawMessage `json:"data"`
	Participant *json.RawMessage `json:"participant"`
	Staff       *json.RawMessage `json:"staff"`
}

func (d *Daemon) uploadCheckData(v []checkResult) error {
	if d.Config.Check.DisableSubmission {
		logger.Warn("skipping submission as requested by configuration")
		return nil
	}
	const q = `mutation CheckResults($objects: [checks_results_insert_input!]!) {
		insert_checks_results(objects: $objects) {
			affected_rows
		}
	}`
	vars := map[string]interface{}{"objects": v}
	_, err := database.GQLQuery(q, vars, d.Config.Database.DefaultQueryRole)
	if err != nil {
		logger.With(err).Error("failed to upload check results")
		return err
	}
	return nil
}
