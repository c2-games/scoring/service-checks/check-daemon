#!/usr/bin/env python3
import enum
import json
from typing import Optional, Union, Any, List


class ResultCode(enum.Enum):
    PASS = 'success'
    FAIL = 'failure'
    WARN = 'partial'
    TIMEOUT = 'timeout'
    UNKNOWN = 'unknown'
    ERROR = 'error'


class ResultJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if hasattr(obj, 'reportJSON'):
            return obj.reportJSON()
        if isinstance(obj, ResultCode):
            return obj.value
        return json.JSONEncoder.default(self, obj)


class Feedback:
    def __init__(self):
        self.feedback = ""
        self._details = []

    @property
    def details(self):
        return self._details

    def add_details(self, val: Union[List, Any]):
        if isinstance(val, list):
            self._details.extend(val)
        else:
            self._details.append(val)

    def __dict__(self):
        return dict(feedback=self.feedback, detailed_results=self._details)


class FinalResult:
    def __init__(self):
        self._result: Optional[ResultCode] = None
        self._participant_result = Feedback()
        self._staff_result = Feedback()

    @property
    def result(self):
        return self._result

    @result.setter
    def result(self, val: ResultCode):
        if not isinstance(val, ResultCode):
            raise ValueError('Result must be a ResultCode enum object')
        self._result = val

    @property
    def feedback(self) -> str:
        return self._participant_result.feedback

    @feedback.setter
    def feedback(self, val: str):
        if not isinstance(val, str):
            raise ValueError('feedback must be a string')
        self._participant_result.feedback = val

    @property
    def staff_feedback(self) -> str:
        return self._staff_result.feedback

    @staff_feedback.setter
    def staff_feedback(self, val: str):
        if not isinstance(val, str):
            raise ValueError('feedback must be a string')
        self._staff_result.feedback = val

    def add_detail(self, detail):
        self._participant_result.add_details(detail)

    def add_staff_detail(self, detail):
        self._staff_result.add_details(detail)

    def success(self, **kwargs):
        self.exit(**kwargs, status=ResultCode.PASS)

    def warn(self, **kwargs):
        self.exit(**kwargs, status=ResultCode.WARN)

    def fail(self, **kwargs):
        self.exit(**kwargs, status=ResultCode.FAIL)

    def unknown(self, **kwargs):
        self.exit(**kwargs, status=ResultCode.UNKNOWN)

    def timeout(self, **kwargs):
        self.exit(**kwargs, status=ResultCode.TIMEOUT)

    def error(self, **kwargs):
        self.exit(**kwargs, status=ResultCode.ERROR)

    def exit(self, status: ResultCode, feedback=None, details=None,
             staff_feedback=None, staff_details=None):
        self.result = status
        if feedback:
            self.feedback = feedback
        if details:
            self.add_detail(details)
        if staff_feedback:
            self.staff_feedback = staff_feedback
        if staff_details:
            self.add_staff_detail(staff_details)

        print(self.json())
        exit(0)

    def json(self):
        return json.dumps(self.__dict__(), cls=ResultJSONEncoder)

    def __dict__(self):
        return {
            'result': self.result,
            'extendedData': {
                "participant": self._participant_result.__dict__(),
                "staff": self._staff_result.__dict__()
            }
        }

