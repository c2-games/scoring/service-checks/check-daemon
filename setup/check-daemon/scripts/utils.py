#!/usr/bin/env python3
from random import randint
from results import FinalResult

def RandResult():
    result = FinalResult()
    ret = randint(0,3)
    if ret == 0:
        SuccessResult()
    elif ret == 1:
        FailureResult()
    else:
        PartialResult()

def SuccessResult():
    result = FinalResult()
    ret = randint(0,3)
    if ret == 0:
        result.success(feedback="All hosts successfully found!")
    elif ret == 1:
        result.success(feedback="Everything is groovy!")
    else:
        result.success(feedback="Online!")

def FailureResult():
    result = FinalResult()
    ret = randint(0,3)
    if ret == 0:
        result.fail(feedback="Maybe try powering off and on again?")
    elif ret == 1:
        result.fail(feedback="Everything is broken!")
    else:
        result.fail(feedback="Offline!")

def PartialResult():
    result = FinalResult()
    ret = randint(0,3)
    if ret == 0:
        result.warn(feedback="Uh, some of the services work sometimes?")
    elif ret == 1:
        result.warn(feedback="Well, sometimes the page loads")
    else:
        result.warn(feedback="All the kerning on the fonts is different!")
