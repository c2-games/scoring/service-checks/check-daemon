// Package cmd implements the main executable functions with commandline input
// Copyright © 2023 Sean Radigan <sradigan@c2games.org>
package cmd

import (
	"check-daemon/checks"
	"check-daemon/config"
	"check-daemon/daemon"
	"check-daemon/database"
	"check-daemon/util"
	"fmt"
	"os"
	"path"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var (
	loggerLevel zap.AtomicLevel
	loggerBasic *zap.Logger
	logger      *zap.SugaredLogger
	cfgFile     string
	checkDaemon *daemon.Daemon
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "check-daemon",
	Short: "Check status and post results",
	Long: `Check on the status of services or challenges:

Post the status and ancillary data to the upstream database.`,
	PreRun: setup,
	Run: func(cmd *cobra.Command, args []string) {
		mainLoop(reload, Cleanup)
	},
}

// Cleanup performs any cleanup on required modules. Call before exit.
func Cleanup() {
	checkDaemon.Cleanup()
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

// SetLogger sets up the internal logger for the cmd package
func SetLogger(loggerIn *zap.Logger) {
	loggerBasic = loggerIn
	logger = loggerBasic.Sugar()
}

// SetLoggerLevel sets the desired log level
// Levels available: debug, info, warn, error, dpanic, panic, fatal
func SetLoggerLevel(level string) {
	switch level {
	case "debug":
		loggerLevel.SetLevel(zap.DebugLevel)
	case "info":
		loggerLevel.SetLevel(zap.InfoLevel)
	case "warn":
		loggerLevel.SetLevel(zap.WarnLevel)
	case "error":
		loggerLevel.SetLevel(zap.ErrorLevel)
	case "dpanic":
		loggerLevel.SetLevel(zap.DPanicLevel)
	case "panic":
		loggerLevel.SetLevel(zap.PanicLevel)
	case "fatal":
		loggerLevel.SetLevel(zap.FatalLevel)
	}
}

func setup(cmd *cobra.Command, args []string) {
	reload()
}

func reload() {
	initConfig()
	cfg := new(config.DaemonConfig)
	if err := viper.Unmarshal(&cfg); err != nil {
		fmt.Println("Unmarshal of config failed:", err)
		os.Exit(1)
	}
	SetLoggerLevel("debug") // Allow all logs
	checkDaemon.StopSelfDestruct()
	checkDaemon.StopChecks()
	checkDaemon.SetConfig(cfg)
	SetLoggerLevel(cfg.Log.Level) // Set to the validated config log level
	logger.Infof("Default config: %s\n", defaultConfigFile())
	checkDaemon.Setup()
	checkDaemon.StartChecks()
}

func defaultConfigPath() string {
	p := path.Join("/", "etc", "check-daemon")
	return p
}

func defaultConfigFile() string {
	p := path.Join(defaultConfigPath(), "config.toml")
	return p
}

func init() {
	util.Setup()
	checkDaemon = daemon.NewDaemon()
	loggerLevel = zap.NewAtomicLevel()
	encoderCfg := zap.NewDevelopmentEncoderConfig()
	logger := zap.New(zapcore.NewCore(
		zapcore.NewJSONEncoder(encoderCfg),
		zapcore.Lock(os.Stderr),
		loggerLevel,
	))
	SetLogger(logger)
	daemon.SetLogger(logger)
	database.SetLogger(logger)
	checks.SetLogger(logger)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", defaultConfigFile(), "config file")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {

		viper.AddConfigPath(defaultConfigPath())
		viper.SetConfigType("toml")
		viper.SetConfigName("config")
	}
	viper.SetDefault("authentication.oidc_scopes", []string{"openid"})

	viper.SetEnvPrefix("CHECK_DAEMON")

	viper.AutomaticEnv() // read in environment variables that match

	envMap := []string{
		"log.level",
		"check.disable_submission",
		"check.scripts_dir",
		"check.interval",
		"check.timeout",
		"check.delay_min",
		"check.delay_max",
		"check.checks",
		"check.teams",
		"check.run_cmd",
		"check.args_tmpl",
		"check.net_iface",
		"database.environment",
		"database.gql_uri",
		"database.default_query_role",
		"database.tls_skip_verify",
		"database.unauthenticated_role",
		"database.request_timeout",
		"authentication.oidc_client_id",
		"authentication.oidc_client_secret",
		"authentication.oidc_scopes",
		"setup.debug_no_clean_up",
		"setup.self_destruct",
		"setup.self_destruct_ttl",
		"setup.self_destruct_variance",
		"setup.submission_batch_size",
		"setup.queue_max",
		"setup.queue_full_strategy",
		"setup.upload_interval",
	}
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	for _, v := range envMap {
		err := viper.BindEnv(v)
		if err != nil {
			fmt.Fprintln(
				os.Stderr,
				"could not setup environment variable mapping:")
			return
		}
	}

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
}
