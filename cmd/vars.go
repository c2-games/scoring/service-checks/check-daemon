// Package cmd implements the main executable functions with commandline input
// Copyright © 2023 Sean Radigan <sradigan@c2games.org>
package cmd

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"check-daemon/checks"

	"github.com/spf13/cobra"
)

// variableInput is a collection of prefix and paths
type variableInput struct {
	Prefix string // prefix for serialization
	Path   string // path of input file
}

// fmtMini Print the output JSON as small as possible
var fmtMini *bool

// inputVarFiles are prefix:path strings e.g. "team:some_vars.yaml"
var inputVarFiles []string

// varsCmd represents the vars command
var varsCmd = &cobra.Command{
	Use:   "vars",
	Short: "Parse and run using check variables",
	Long: `Parse and format input files containing check variables,
then run a check script with the inputs.`,
	Run: func(cmd *cobra.Command, args []string) {
		var inputs []variableInput
		for _, i := range inputVarFiles {
			parts := strings.SplitN(i, ":", 2)
			if len(parts) < 2 {
				fmt.Fprintf(os.Stderr, "warning: invalid format, ignoring input: %s", i)
				continue
			}
			inputs = append(inputs, variableInput{parts[0], parts[1]})
		}
		processInputs(inputs)
	},
}

func processInputs(inputs []variableInput) {
	var varsContent []checks.CheckVariable
	for _, i := range inputs {
		// Read in the file content
		content, err := os.ReadFile(i.Path)
		if err != nil {
			fmt.Fprintf(os.Stderr, "error reading file: %s", i.Path)
			continue
		}
		varsContent = append(
			varsContent,
			checks.CheckVariable{
				Prefix:  i.Prefix,
				Content: json.RawMessage(content),
			},
		)
	}
	// Merge everything down into a single JSON output
	out, err := checks.SerializeVariables(varsContent, !*fmtMini)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error serializing: %s", err.Error())
	}
	fmt.Printf("%s\n", string(out))
}

func init() {

	rootCmd.AddCommand(varsCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// varsCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	varsCmd.Flags().StringSliceVarP(
		&inputVarFiles, "input", "i", []string{},
		"Input file e.g. check:/path/to/file.yml, team:/path/to/file.json",
	)
	varsCmd.MarkFlagRequired("input")
	fmtMini = varsCmd.Flags().BoolP("minify", "m", false, "Minify JSON output")
}
