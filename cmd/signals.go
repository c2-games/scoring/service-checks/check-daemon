package cmd

import (
	"os"
	"os/signal"
	"syscall"
)

var (
	shutdown      = false
	reloadPending = false
	signalChan    = make(chan os.Signal, 1)
	exitChan      = make(chan int)
	interruptChan = make(chan bool, 1)
)

func handleSignal(s os.Signal) {
	logger.Debugf("Handling: %s", s.String())
	switch s {
	// kill -SIGHUP XXXX (1)
	case syscall.SIGHUP:
		if !reloadPending {
			logger.Info("Reloading settings")
			reloadPending = true
			interruptChan <- true
		}

	// kill -SIGINT XXXX or Ctrl+c (2)
	case syscall.SIGINT:
		fallthrough
	// kill -SIGTERM XXXX (15)
	case syscall.SIGTERM:
		logger.Infof("Caught %s - please wait for graceful shutdown", s.String())
		shutdown = true
		interruptChan <- true

	default:
		logger.Debugf("Unknown signal: %s", s.String())
	}
}

func setSignalHooks(sigchan chan os.Signal) {
	signal.Notify(sigchan,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
	)
}

func handleSignals() {
	for {
		sig := <-signalChan
		handleSignal(sig)
	}
}

func mainLoop(reload func(), cleanup func()) {
	setSignalHooks(signalChan)

	go handleSignals()

	go func() {
		for {
			select {
			case <-interruptChan:
				if shutdown {
					cleanup()
					exitChan <- 0
					return
				}
				if reloadPending {
					reload()
					reloadPending = false
				}
			}
		}
	}()

	logger.Debug("waiting on exit channel")
	code := <-exitChan
	os.Exit(code)
}
