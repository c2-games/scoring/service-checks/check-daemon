package util

import (
	"encoding/json"
	"math/rand"
	"time"
)

// Setup sets up the util package
func Setup() {
	rand.Seed(time.Now().UnixNano())
}

// PrettyStruct formats any struct to indented JSON for printing
func PrettyStruct(data interface{}) (string, error) {
	val, err := json.MarshalIndent(data, "", "    ")
	if err != nil {
		return "", err
	}
	return string(val), nil
}

// StringListContains checks if a string is in a list of strings
func StringListContains(list []string, value string) bool {
	for _, v := range list {
		if v == value {
			return true
		}
	}
	return false
}

// GenerateRandom generates a random number in a range
func GenerateRandom(min int, max int) int {
	// If there is no range, just return the value
	if min == max {
		return min
	}
	return rand.Intn(max-min+1) + min
}
