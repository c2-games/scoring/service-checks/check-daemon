# Build the check daemon
FROM golang:1.23.5-alpine AS daemon-builder
LABEL maintainer="c2games"

ENV APP_DIR=/tmp/build

WORKDIR $APP_DIR

COPY . $APP_DIR/

RUN go install -ldflags "-s -w" -v ./... && \
    go clean -r -cache -modcache

# Build the actual runtime container
FROM alpine:3.20

ARG NAME="check-daemon"
ARG DESCRIPTION="C2Games service and challenge check daemon"
ARG MAINTAINER="c2games.org <support@c2games.org>"
ARG VENDOR="c2games"
ARG ARCH=amd64
ARG DOCKER_CMD="docker run -t --rm registry.gitlab.com/c2-games/scoring/service-checks/check-daemon"
ARG DOCKER_CMD_HELP="N/A"
ARG IMAGE_USAGE="https://gitlab.com/c2-games/scoring/service-checks/check-daemon/-/blob/main/README.md"
ARG VCS_URL="https://gitlab.com/c2-games/scoring/service-checks/check-daemon/"
ARG VCS_REF
ARG VCS_TIMESTAMP
ARG VERSION=v0.1
ARG BUILD_DATE

RUN apk add --no-cache --update python3

# Copy the binary from the build container
COPY --from=daemon-builder /go/bin/check-daemon /usr/local/bin/check-daemon

ENTRYPOINT  ["/usr/local/bin/check-daemon"]
CMD []
