package queue

import (
	"fmt"
	"container/list"
	"sync"
)

type Queue[T any] struct {
	mu            *sync.Mutex
	data          *list.List
	MaxSize      int
	FullStrategy string
}

func NewQueue[T any]() *Queue[T] {
	return &Queue[T]{
		mu: &sync.Mutex{},
		data: list.New(),
	}
}

func (q *Queue[T]) Len() int {
	q.mu.Lock()
	defer q.mu.Unlock()
	return q.data.Len()
}

func (q *Queue[T]) enqueueUnsafe(input T) {
	if q.MaxSize == 0 {
		panic("enqueue not possible for MaxSize 0")
	} else if q.MaxSize > 0 && q.data.Len() >= q.MaxSize {
		switch q.FullStrategy {
		case "discard_oldest":
			e := q.data.Front()
			q.data.Remove(e)
		case "discard_newest":
			e := q.data.Back()
			q.data.Remove(e)
		default:
			panic(fmt.Sprintf("invalid full strategy: %s", q.FullStrategy))
		}
	}
	// negative max is unlimited queue size
	q.data.PushBack(input)
}

func (q *Queue[T]) Enqueue(input T) {
	q.mu.Lock()
	defer q.mu.Unlock()
	q.enqueueUnsafe(input)
}

func (q *Queue[T]) EnqueueMulti(inputs []T) {
	q.mu.Lock()
	defer q.mu.Unlock()
	for _, v := range inputs {
		q.enqueueUnsafe(v)
	}
}

func (q *Queue[T]) dequeueUnsafe() (T, bool) {
	if q.data.Len() == 0 {
		var tmp T
		return tmp, false
	}

	e := q.data.Front()
	q.data.Remove(e)
	// Cast e.Value(any) to T
	value := e.Value.(T) // panic if not T
	return value, true
}

func (q *Queue[T]) Dequeue() (T, bool) {
	q.mu.Lock()
	defer q.mu.Unlock()
	return q.dequeueUnsafe()
}

func (q *Queue[T]) DequeueMulti(count int) ([]T, bool) {
	results := []T{}
	q.mu.Lock()
	defer q.mu.Unlock()
	for i := 0; i < count; i++ {
		r, ok := q.dequeueUnsafe()
		if !ok {
			break
		}
		results = append(results, r)
	}
	return results, len(results) == count
}
