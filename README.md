# C2Games Service and Challenge check daemon

## Setup

### Keycloak

**Note:** Ignore keycloak setup if using the c2games `dev` realm keycloak and
just retrieve the client id and secret from an administrator.

As an administrator in keycloak, create a new confidential client. Copy the the
client name (`oidc_client_id`) and the client secret (`oidc_client_secret`)
into the config file.

In Settings, enable the service account and press save. Under the new
"Service Account Roles" tab, add the hasura client role required.

In Mappers, add the following:

1. `x-hasura-default-role`
   1. Mapper Type: `Hardcoded claim`
   2. Token Claim Name: `https://hasura\.io/jwt/claims.x-hasura-default-role`
   3. Value: `scoringpod` (or anything else desired)
   4. Claim JSON Type: `string`
2. `x-hasura-allowed-roles`
   1. Mapper Type: `User Client Role`
   2. Client ID: `hasura`
   3. Multivalued: `ON`
   4. Token Claim Name: `https://hasura\.io/jwt/claims.x-hasura-allowed-roles`
   5. Claim JSON Type: `string`

### Hasura

Clone the C2Games hasura repo, anywhere is fine, it does not need
to be nested in this project:

```sh
git clone https://gitlab.com/c2-games/scoring/hasura
```

Start the hasura instance on the `c2games` network in docker so it is
accessible by this project.

```sh
cd hasura
docker-compose up -d
```

Optionally, apply seed data to the database for testing:

```sh
utils/apply-hasura-test-seed.sh
```

## Usage

Use the `Dockerfile` provided to generate an image. Configure the daemon to
check based on `checks` and `teams` in the configuration file.

See example config provided in `setup/check-daemon/config/config.toml`.

Configurations may also be set using the environment using an uppercase and
underscore transform such as the following:

* CHECK_DAEMON_CHECK_TEAMS="asdf,qwerty"
  * Maps to

    ```toml
    [check]
    teams = ["asdf", "qwerty"]
    ```

* CHECK_DAEMON_CHECK_SCRIPTS_DIR=./setup/check-daemon/scripts
  * Maps to

    ```toml
    [check]
    scripts_dir = "./setup/check-daemon/scripts"
    ```

* CHECK_DAEMON_CHECK_NET_IFACE=enp42s0
  * Maps to

    ```toml
    [check]
    net_iface = "enp42s0"
    ```

### Check variable parsing

Use the subcommand `vars` to parse various json files, see
`check-daemon vars --help` for more details. Prefix the files to be read with
`event`/`team`/`check`/`deployment` to generate a test file that is compatible
with a check script. Specify the `-i` argument as many times as you want, the
command only handles one of each type.

```sh
./check-daemon vars -i team:asdf2.json -i check:asdf.json
{
    "check": {
        "blarg": "foo"
    },
    "team": {
        "domains": {
            "files.team{TEAM}.ncaecybergames.org": "172.18.14.{TEAM}",
            "ns1.team{TEAM}.ncaecybergames.org": "172.18.13.{TEAM}",
            "shell.team{TEAM}.ncaecybergames.org": "172.18.14.{TEAM}",
            "www.team{TEAM}.ncaecybergames.org": "172.18.13.{TEAM}"
        }
    }
}
```

In a competition environment, the variables will be pulled from hasura as
JSON then merged into a single json object.

The check script will optionally have the file templated into the calling
arguments as defined in hasura. An example of this would be:

```txt
--vars {{.VarsFile}}
```

Which would result in a call such as:

```sh
check_icmp.py --vars /tmp/check_external_icmp_Team10_2868678570/vars.json
```

## Developing

Rebuild and start the `check-daemon` container using `docker-compose`

```sh
docker-compose up --build -d
```
