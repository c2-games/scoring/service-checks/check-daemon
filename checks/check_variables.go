package checks

import (
	"encoding/json"

	"go.uber.org/zap"
)

var (
	loggerBasic *zap.Logger
	logger      *zap.SugaredLogger
)

// SetLogger sets up the logging for the daemon package
func SetLogger(loggerIn *zap.Logger) {
	loggerBasic = loggerIn
	logger = loggerBasic.Sugar()
}

// CheckVariable is a pairing of prefix and json data
type CheckVariable struct {
	Prefix  string
	Content json.RawMessage
}

// SerializeVariables readies variables to be written as JSON
//
// If no error, bytes returned are marshaled json data
func SerializeVariables(inputs []CheckVariable, pretty bool) ([]byte, error) {
	out := make(map[string]json.RawMessage)
	for _, i := range inputs {
		out[i.Prefix] = i.Content
	}
	if pretty {
		return json.MarshalIndent(out, "", "    ")
	}
	return json.Marshal(out)
}
