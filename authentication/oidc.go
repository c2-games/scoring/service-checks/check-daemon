package authentication

import (
	"check-daemon/config"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"mime"
	"net/http"
	"strings"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"
)

type oidcProviderJSON struct {
	Issuer      string   `json:"issuer"`
	AuthURL     string   `json:"authorization_endpoint"`
	TokenURL    string   `json:"token_endpoint"`
	UserInfoURL string   `json:"userinfo_endpoint"`
	Algorithms  []string `json:"id_token_signing_alg_values_supported"`
}

var (
	oidcProviderCfg *oidcProviderJSON
	oidcSettings    *config.AuthConfig
	clientToken     *oauth2.Token
)

func authReady() bool {
	return oidcProviderCfg != nil
}

// GetClientAuthToken returns a JWT token for the configured client
func GetClientAuthToken() (*oauth2.Token, error) {
	if !authReady() {
		return nil, fmt.Errorf("oidc not initialized yet")
	}
	if clientToken != nil && clientToken.Valid() {
		return clientToken, nil
	}
	oauthCfg := &clientcredentials.Config{
		// Set client ID and client secret here
		ClientID:     oidcSettings.OIDCClientID,
		ClientSecret: oidcSettings.OIDCClientSecret,
		TokenURL:     oidcProviderCfg.TokenURL,
		Scopes:       oidcSettings.OIDCScopes,
	}
	clientToken, err := oauthCfg.Token(context.Background())
	if err != nil {
		return nil, err
	}
	return clientToken, nil
}

// SetupOIDC configures the authentication module
func SetupOIDC(cfg *config.AuthConfig) error {
	wellKnownURL := strings.TrimRight(cfg.OIDCCfgURL, "/") + "/.well-known/openid-configuration"
	// Set Issuer information here
	err := initOIDCConfig(wellKnownURL)
	if err != nil {
		panic("failed to initialize OIDC: " + err.Error())
	}
	oidcSettings = cfg

	return nil
}

func initOIDCConfig(cfgURL string) error {
	if oidcProviderCfg != nil {
		return nil // This should only run once, NOOP if run again
	}
	// Query the oidc provider for the configuration
	req, err := http.NewRequest("GET", cfgURL, nil)
	if err != nil {
		return err
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("unable to read response body: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("%s: %s", resp.Status, body)
	}

	// Create a pointer to hold the internal oidc config from the provider
	oidcProviderCfg = new(oidcProviderJSON)
	// Parse the json body of the http response into the config variable
	err = unmarshalResp(resp, body, oidcProviderCfg)
	if err != nil {
		oidcProviderCfg = nil
		return fmt.Errorf("oidc: failed to decode provider discovery object: %v", err)
	}
	return nil
}

// Stole this from github.com/coreos/go-oidc
func unmarshalResp(r *http.Response, body []byte, v interface{}) error {
	err := json.Unmarshal(body, &v)
	if err == nil {
		return nil
	}
	ct := r.Header.Get("Content-Type")
	mediaType, _, parseErr := mime.ParseMediaType(ct)
	if parseErr == nil && mediaType == "application/json" {
		return fmt.Errorf("got Content-Type = application/json, but could not unmarshal as JSON: %v", err)
	}
	return fmt.Errorf("expected Content-Type = application/json, got %q: %v", ct, err)
}
