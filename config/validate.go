package config

import (
	"check-daemon/util"
	"fmt"
	"html/template"
	"os"
	"reflect"
	"time"
)

func getCfgFieldName(i any, field string) string {
	f, _ := reflect.TypeOf(i).Elem().FieldByName(field)
	return f.Tag.Get("mapstructure")
}

// Validate will check if the configuration is valid
func (c *LogConfig) Validate() error {
	valid := []string{"debug", "info", "warn", "error", "dpanic", "panic", "fatal"}
	if util.StringListContains(valid, c.Level) {
		return nil
	}
	return fmt.Errorf("invalid log level: \"%s\"", c.Level)
}

// Validate will check if the configuration is valid
func (c *DatabaseConfig) Validate() error {
	if c.Environment == "" {
		field := getCfgFieldName(c, "Environment")
		return fmt.Errorf("required value %s must be set", field)
	}
	if c.GqlURI == "" {
		field := getCfgFieldName(c, "GqlURI")
		return fmt.Errorf("required value %s must be set", field)
	}
	if c.DefaultQueryRole == "" {
		field := getCfgFieldName(c, "DefaultQueryRole")
		return fmt.Errorf("required value %s must be set", field)
	}
	if c.UnauthenticatedRole == "" {
		field := getCfgFieldName(c, "UnauthenticatedRole")
		return fmt.Errorf("required value %s must be set", field)
	}
	return nil
}

// Validate will check if the configuration is valid
func (c *AuthConfig) Validate() error {
	if c.OIDCClientID == "" {
		field := getCfgFieldName(c, "OIDCClientID")
		return fmt.Errorf("required value %s must be set", field)
	}
	if c.OIDCClientSecret == "" {
		field := getCfgFieldName(c, "OIDCClientSecret")
		return fmt.Errorf("required value %s must be set", field)
	}
	return nil
}

// Validate will check if the configuration is valid
func (c *CheckConfig) Validate() error {
	info, err := os.Stat(c.ScriptsDir)
	if err != nil {
		field := getCfgFieldName(c, "ScriptsDir")
		return fmt.Errorf("could not access %s \"%s\": %w", field, c.ScriptsDir, err)
	}
	if !info.IsDir() {
		field := getCfgFieldName(c, "ScriptsDir")
		return fmt.Errorf("%s \"%s\" is not a directory", field, c.ScriptsDir)
	}
	if c.Interval < 15*time.Second {
		field := getCfgFieldName(c, "Interval")
		return fmt.Errorf("%s must be greater than 15s", field)
	}
	if c.Timeout < 5*time.Second {
		field := getCfgFieldName(c, "Timeout")
		if c.Timeout != 0*time.Second {
			return fmt.Errorf("%s must be at least 5s or 0s for infinite", field)
		}
	}
	if c.DelayMin > c.DelayMax {
		maxField := getCfgFieldName(c, "DelayMax")
		minField := getCfgFieldName(c, "DelayMin")
		return fmt.Errorf("%s must be less than or equal to %s", minField, maxField)
	}
	if c.DelayMax >= c.Interval {
		delayField := getCfgFieldName(c, "DelayMax")
		intervalField := getCfgFieldName(c, "Interval")
		return fmt.Errorf("%s must be less than %s", delayField, intervalField)
	}
	if len(c.Checks) < 1 {
		field := getCfgFieldName(c, "Checks")
		return fmt.Errorf("at least 1 check must be specified in %s", field)
	}
	if len(c.Teams) < 1 {
		field := getCfgFieldName(c, "Teams")
		return fmt.Errorf("at least 1 team must be specified in %s", field)
	}
	if len(c.RunCmd) < 1 {
		field := getCfgFieldName(c, "RunCmd")
		return fmt.Errorf("%s must not be empty", field)
	}
	if len(c.ArgsTmpl) < 1 {
		field := getCfgFieldName(c, "ArgsTmpl")
		return fmt.Errorf("%s must not be empty", field)
	}
	_, err = template.New("cmd").Parse(c.ArgsTmpl)
	if err != nil {
		field := getCfgFieldName(c, "ArgsTmpl")
		return fmt.Errorf("could not parse template in %s: %w", field, err)
	}
	if len(c.NetIface) < 1 {
		field := getCfgFieldName(c, "NetIface")
		return fmt.Errorf("%s must not be empty", field)
	}
	return nil
}

// Validate will check if the configuration is valid
func (c *SetupConfig) Validate() error {
	ttlField := getCfgFieldName(c, "SelfDestructTTL")
	if c.SelfDestructTTL < 1*time.Minute {
		return fmt.Errorf("%s must be at least 1 minute", ttlField)
	}
	if c.SelfDestructVariance >= c.SelfDestructTTL {
		varianceField := getCfgFieldName(c, "SelfDestructVariance")
		return fmt.Errorf("%s must be less than %s", varianceField, ttlField)
	}
	validStrategies := []string{
		"discard_oldest",
		"discard_newest",
	}
	if !util.StringListContains(validStrategies, c.QueueFullStrategy) {
		queueFullStrategyField := getCfgFieldName(c, "QueueFullStrategy")
		return fmt.Errorf("%s must be one of %v", queueFullStrategyField, validStrategies)
	}
	// These are valid, but printing a warning might be helpful
	if c.SubmissionBatchSize == 0 {
		submissionBatchSizeField := getCfgFieldName(c, "SubmissionBatchSize")
		fmt.Fprintf(os.Stderr, "warning: %s is 0, submissions are disabled\n", submissionBatchSizeField)
	}
	if c.QueueMax == 0 {
		queueMaxField := getCfgFieldName(c, "QueueMax")
		fmt.Fprintf(os.Stderr, "warning: %s is 0, queuing is disabled\n", queueMaxField)
	}
	if c.UploadInterval < 1*time.Second {
		uploadIntervalField := getCfgFieldName(c, "UploadInterval")
		fmt.Fprintf(os.Stderr, "warning: %s is less than 1 second, submissions are disabled\n", uploadIntervalField)
	}
	return nil
}

// Validate will check if the configuration is valid
func (c *DaemonConfig) Validate() error {
	err := c.Log.Validate()
	if err != nil {
		field := getCfgFieldName(c, "Log")
		return fmt.Errorf("invalid \"%s\" config: %w", field, err)
	}
	err = c.Database.Validate()
	if err != nil {
		field := getCfgFieldName(c, "Database")
		return fmt.Errorf("invalid \"%s\" config: %w", field, err)
	}
	err = c.Authentication.Validate()
	if err != nil {
		field := getCfgFieldName(c, "Authentication")
		return fmt.Errorf("invalid \"%s\" config: %w", field, err)
	}
	err = c.Check.Validate()
	if err != nil {
		field := getCfgFieldName(c, "Check")
		return fmt.Errorf("invalid \"%s\" config: %w", field, err)
	}
	err = c.Setup.Validate()
	if err != nil {
		field := getCfgFieldName(c, "Setup")
		return fmt.Errorf("invalid \"%s\" config: %w", field, err)
	}
	if c.Setup.SelfDestructTTL <= c.Check.Interval {
		setupField := getCfgFieldName(c, "Setup")
		checkField := getCfgFieldName(c, "Check")
		ttlField := getCfgFieldName(&c.Setup, "SelfDestructTTL")
		intervalField := getCfgFieldName(&c.Check, "Interval")
		return fmt.Errorf("%s.%s must be greater than %s.%s", setupField, ttlField, checkField, intervalField)
	}
	return nil
}
