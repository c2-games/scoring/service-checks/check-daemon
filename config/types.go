package config

import "time"

// LogConfig holds the configuration options for logging
type LogConfig struct {
	Level string `mapstructure:"level"`
}

// DatabaseConfig holds the configuration options for the database connection
type DatabaseConfig struct {
	Environment         string        `mapstructure:"environment"`
	GqlURI              string        `mapstructure:"gql_uri"`
	DefaultQueryRole    string        `mapstructure:"default_query_role"`
	TLSSkipVerify       bool          `mapstructure:"tls_skip_verify"`
	RequestTimeout      time.Duration `mapstructure:"request_timeout"`
	UnauthenticatedRole string        `mapstructure:"unauthenticated_role"`
}

// AuthConfig holds the configuration options for the database authentication
type AuthConfig struct {
	OIDCCfgURL       string   `mapstructure:"oidc_cfg_url"`
	OIDCClientID     string   `mapstructure:"oidc_client_id"`
	OIDCClientSecret string   `mapstructure:"oidc_client_secret"`
	OIDCScopes       []string `mapstructure:"oidc_scopes"`
}

// CheckConfig holds the configuration options for the check execution runtime
type CheckConfig struct {
	DisableSubmission bool          `mapstructure:"disable_submission"`
	ScriptsDir        string        `mapstructure:"scripts_dir"`
	Interval          time.Duration `mapstructure:"interval"`
	Timeout           time.Duration `mapstructure:"timeout"`
	DelayMin          time.Duration `mapstructure:"delay_min"`
	DelayMax          time.Duration `mapstructure:"delay_max"`
	Checks            []string      `mapstructure:"checks"`
	Teams             []string      `mapstructure:"teams"`
	RunCmd            []string      `mapstructure:"run_cmd"`
	ArgsTmpl          string        `mapstructure:"args_tmpl"`
	NetIface          string        `mapstructure:"net_iface"`
}

// SetupConfig holds values for daemon operation
type SetupConfig struct {
	DebugNoCleanUp       bool          `mapstructure:"debug_no_clean_up"`
	SelfDestruct         bool          `mapstructure:"self_destruct"`
	SelfDestructTTL      time.Duration `mapstructure:"self_destruct_ttl"`
	SelfDestructVariance time.Duration `mapstructure:"self_destruct_variance"`
	SubmissionBatchSize  int           `mapstructure:"submission_batch_size"`
	QueueMax             int           `mapstructure:"queue_max"`
	QueueFullStrategy    string        `mapstructure:"queue_full_strategy"`
	UploadInterval       time.Duration `mapstructure:"upload_interval"`
}

// DaemonConfig is the superset of configuration options
type DaemonConfig struct {
	Log            LogConfig      `mapstructure:"log"`
	Database       DatabaseConfig `mapstructure:"database"`
	Authentication AuthConfig     `mapstructure:"authentication"`
	Check          CheckConfig    `mapstructure:"check"`
	Setup          SetupConfig    `mapstructure:"setup"`
}
